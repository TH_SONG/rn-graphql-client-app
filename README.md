# GraphQL RN App

* React Native app with GraphQL Apollo
* It shows simple query result

## Setting
* Copy env file and edit server's endpoint URI.
```bash
cp env.example.js env.js
vi env.js
```
